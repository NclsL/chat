import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
import Chatview from './Chatview';

class App extends Component {
  render() {
    return (
      <>
        <Chatview />
      </>
    );
  }
}

export default App;
