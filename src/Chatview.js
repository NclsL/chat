import React from 'react';
import io from 'socket.io-client';
import {
  Input,
  Segment,
  Button,
  Divider,
  Modal,
  Icon,
  Header,
  Form
} from "semantic-ui-react";

const Message = props => (
  <>
    <p><span style={{fontWeight: "bold"}}>{props.message.author}:</span> {props.message.content}</p>
    <Divider />
  </>
);

export default class Chatview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      messageContent: '',
      messages: [],
      displayModal: true
    };
  }

  sendMessage = () => {
    this.socket.emit('SEND_CHAT_MSG', {
      author: this.state.username,
      content: this.state.messageContent
    });
    this.setState({messageContent: ''});
  }

  connect = () => {
    this.socket = io('localhost:8001');
    this.socket.on('RECV_CHAT_MSG', (msg) => {
      this.setState({messages: [...this.state.messages, msg]});
    });

    if (this.state.username) {
      this.setState({
	displayModal: false
      });
    }
  }

  disconnect = () => {
    this.socket.disconnect();
    this.setState({
      username: '',
      messageContent: '',
      messages: [],
      displayModal: true
    });
  }

  render() {
    return (
      <div>
	{
	  this.state.username && !this.state.displayModal ?
	    <div style={{ paddingTop: "1rem", maxWidth: "70rem", margin: "auto"}}>
	      <div style={{paddingBottom: "1rem"}}>
		<Button basic floated="right" color="red" onClick={(e) => this.disconnect()}>Disconnect</Button>
		<Header content="Budget IRC"/>
		<span style={{fontStyle: "italic", color: "grey"}}>Hit enter to send messages</span>
	      </div>
	      <Segment style={{overflow: 'auto', height: "50rem",}}>
		{
		  this.state.messages.map((msg, idx) => {
		    return (
		      <Message message={msg} key={idx} />
		    );
		  })
		}
	      </Segment >
	      <Form onSubmit={() => this.sendMessage()}>
		<Form.Input label={this.state.username} fluid value={this.state.messageContent} onChange={(e) => this.setState({ messageContent: e.target.value })}/>
	      </Form>
	    </div>
	  :
	  <>
	    <Modal open={this.state.displayModal} basic size="small">
	      <Header icon='archive' content='Set a username to continue' />
	      <Modal.Content>
		<Input fluid onChange={(e) => this.setState({username: e.target.value})}></Input>
	      </Modal.Content>
	      <Modal.Actions>
		<Button color='green'
			inverted
			onClick={(e) => this.connect()}
		>
		  <Icon name='checkmark' /> GO
		</Button>
	      </Modal.Actions>
	    </Modal>
	  </>
	}
      </div>
    );
  }
}
