const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

server.listen(8001, () => console.log('listening on 8001'));

io.on('connection', (socket) => {
  socket.on('SEND_CHAT_MSG', (msg) => {
    io.emit('RECV_CHAT_MSG', msg);
  });
});
